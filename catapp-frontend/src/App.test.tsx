import React from 'react';
import * as path from 'path';
import * as fs from 'fs';
import { render, screen, waitFor } from '@testing-library/react';
import App from './App';
import { rest } from 'msw';
import { setupServer } from 'msw/node'

const server = setupServer(
    rest.get('/cat', (req, res, ctx) => {
        // Read the cat image from the file system using the "fs" module.
        const imageBuffer = fs.readFileSync(
            path.resolve(__dirname, 'cat_test.jpg'),
        )
        return res(
            ctx.set('Content-Length', imageBuffer.byteLength.toString()),
            ctx.set('Content-Type', 'image/jpeg'),
            // Respond with the "ArrayBuffer".
            ctx.body(imageBuffer),
        )
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())


test('inform visitors that the website is all about cats', () => {
    render(<App url='/cat' />);
    expect(screen.getByText(/cat/i)).toBeInTheDocument();
});

test('that the website displays a cat image', async () => {
    render(<App url='/cat' />);
    await waitFor(() => screen.getByAltText('A cat.'));
    expect(screen.getByAltText('A cat.')).toBeInTheDocument();
});
