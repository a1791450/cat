import React, { Component } from 'react';
import './Cat.css';
import axios from 'axios';

class Cat extends Component {
  state = {
    url : ""
  };

  componentDidMount() {
    this.fetchPic();
  }

  async fetchPic() {
    const values = await axios.get('https://api.thecatapi.com/v1/images/search', {
      headers : {
        "x-api-key" : "568277e9-17f7-4dcb-9ab3-a54c563541e8"
      }
    });
    console.log('values', values.data[0].url)
    this.setState({ url: values.data[0].url });
    console.log('state.url ', this.state.url)
  }

  handleSubmit = async (event: any) => {
    this.fetchPic()
  };


  render() {
    return (
      <div>
        <h1>Hi there, here's your cat</h1>
        <div className='cat-div'>
          <img src={this.state.url} alt='a cat should be here' />
        </div>
        
        <button onClick={this.handleSubmit}>NEXT CAT</button>
      </div>
    );
  }
}

export default Cat;
