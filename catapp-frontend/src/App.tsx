import React from "react";
import "./App.css";
import Cat from './Cat'

type AppProps = {
    url: string;
};

function App({ url }: AppProps) {
    return (
        <div className="App">
            <h1 className="App-title">
                <p>Hello!</p>
                <p>Here's a cat:</p>
            </h1>
            <img src={url} alt="A cat."></img>
            <Cat></Cat>
        </div>

    );
}

export default App;
