const express = require('express')
const app = express()

// Used to service images as static
app.use(express.static('images'))

// Proper route to api call
app.get('/api/cat', (req, res) => {
  res.sendFile('cat.jpg', { root: 'images/' });
})

// Redirect everything else to proper route
app.get('/*', (req, res) => {
  res.redirect('/api/cat');
})

module.exports = app;