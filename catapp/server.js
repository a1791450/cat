const app = require("./app");
const port = 3000

//Gives access point/call route
app.listen(port, () => {
    console.log(`App hosted locally at http://localhost:${port}/api/cat`)
})