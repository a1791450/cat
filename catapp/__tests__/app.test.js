const request = require("supertest");
const app = require("../app");

describe("Test the main path", () => {
    test("It should returns an image", () => {
        return request(app)
            .get("/api/cat")
            .expect('Content-Type', /image/)
            .expect(200);
    });
});

describe("Test redirection", () => {
    test("It should redirects to main path", () => {
        return request(app)
            .get("/random")
            .expect(302)
            .expect('Location', '/api/cat');
    });
});